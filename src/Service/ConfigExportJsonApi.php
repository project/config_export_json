<?php

namespace Drupal\config_export_json\Service;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Component\Serialization\Json;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * ConfigExportJsonApi Service Class.
 */
class ConfigExportJsonApi {

  use StringTranslationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  public $config;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File System.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory, FileSystemInterface $file_system) {
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->config = $config_factory->getEditable('config_export_json.settings');
    $this->logger = $logger_factory->get('config_export_json');
    $this->fileSystem = $file_system;
  }

  /**
   * Method to add config to JSON.
   *
   * @param array $projectConfigs
   *   Configs to be added.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function add(array $projectConfigs) {

    foreach ($projectConfigs as $projectKey => $configs) {

      // Check if config already exists.
      if (!empty($this->config->get($projectKey))) {
        $currentConfig = $this->config->get($projectKey);
        $configs = array_merge($currentConfig, $configs);
      }

      $this->config->set($projectKey, $configs);
      $this->config->save();
    }

    return TRUE;
  }

  /**
   * Method to generate Json File.
   *
   * @param bool $log
   *   Flag indicating if errors should be logged.
   *
   * @return false|string
   *   Path of generated file, False if file was not generated.
   */
  public function exportJsonFile($log = FALSE) {

    $filesystem = new Filesystem();

    // Get JSON data to export.
    $json = $this->getJson();

    if (empty($json)) {
      $this->logger->error($this->t('Error on get JSON'));
      return FALSE;
    }

    // Get public folder.
    $publicPath = PublicStream::basePath();
    $path = $this->fileSystem->realpath($publicPath);
    $path .= '/config';

    // Create directory if not exists.
    if (empty($filesystem->exists($path))) {

      // Check if directory was created.
      try {
        $filesystem->mkdir($path);
      }
      catch (IOExceptionInterface $exception) {

        $this->logger->error($this->t('Error creating folder: @folder', [
          '@folder' => $path,
        ]));

        return FALSE;
      }
    }

    // File name.
    $fileName = 'config.json';

    // Full path.
    $fullPath = $path . '/' . $fileName;

    // Delete old file.
    if ($filesystem->exists($fullPath)) {
      $filesystem->remove($fullPath);
    }

    // Save the new file.
    try {
      $filesystem->dumpFile($fullPath, $json);
    }
    catch (IOExceptionInterface $exception) {
      $this->logger->error($this->t('Error on export file'));
      return FALSE;
    }

    if ($log) {
      $this->logger->info($this->t('Json Config File has been exported. Path: @path', [
        '@path' => $fullPath,
      ]));
    }

    // Return file path.
    return $fullPath;
  }

  /**
   * Method to get JSON.
   *
   * @param bool $jsonEncode
   *   Flag indicating if configs should be encoded.
   *
   * @return array|false|string
   *   Encoded JSON of config as array.
   */
  public function getJson($jsonEncode = TRUE) {
    $configs = $this->config->getRawData();

    // Include exposed settings configured in UI.
    $exposed = $this->configFactory->get('config_export_json.config')->get('exposed');
    $lines = array_filter(preg_split("/[\r\n]+/", $exposed));
    foreach ($lines as $line) {
      $line = explode(':', $line);
      $name = $line[0];
      $key = isset($line[1]) ? $line[1] : NULL;

      // The whole config is exported.
      if (empty($key)) {
        $configs[$name] = $this->configFactory->get($name)->getRawData();
        continue;
      }

      // Only specific configuration is exported.
      $configs[$name][$key] = $this->configFactory->get($name)->get($key);
    }

    if (empty($configs)) {
      return FALSE;
    }

    if (!$jsonEncode) {
      return $configs;
    }

    return Json::encode($configs);
  }

}
