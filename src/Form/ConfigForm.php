<?php

namespace Drupal\config_export_json\Form;

use Drupal\config_export_json\Service\ConfigExportJsonApi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form Class.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Config Export Json API.
   *
   * @var \Drupal\config_export_json\Service\ConfigExportJsonApi
   */
  private $configExportJson;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigExportJsonApi $config_export_json) {
    parent::__construct($config_factory);
    $this->configExportJson = $config_export_json;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config_export_json.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_export_json_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_export_json.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('config_export_json.config');

    $form['exposed'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Configs to be exposed'),
      '#description' => $this->t('List all configurations (one per line) that should be exposed. Exemple: <em>system.site:name</em>'),
      '#default_value' => $config->get('exposed'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('config_export_json.config')
      ->set('exposed', $form_state->getValue('exposed'))
      ->save();

    // Update the exported file.
    $this->configExportJson->exportJsonFile(TRUE);

    parent::submitForm($form, $form_state);
  }

}
