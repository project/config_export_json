<?php

namespace Drupal\config_export_json\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "config_json_rest_resource",
 *   label = @Translation("Config Export JSON"),
 *   uri_paths = {
 *     "canonical" = "/api/config.json"
 *   }
 * )
 */
class ConfigJsonRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Config Export to JSON.
   *
   * @var Drupal\config_export_json\Service\ConfigExportJsonApi
   */
  private $configExportJson;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('config_export_json');
    $instance->currentUser = $container->get('current_user');
    $instance->configExportJson = $container->get('config_export_json.api');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $payload = [];
    if (!empty($this->configExportJson->getJson(FALSE))) {
      $payload = $this->configExportJson->getJson(FALSE);
    }
    $response = new ResourceResponse($payload, 200);
    $response->addCacheableDependency($this->configFactory->get('config_export_json.settings'));

    return $response;
  }

}
